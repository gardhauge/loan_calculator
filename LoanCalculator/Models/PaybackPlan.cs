﻿using System;
using System.Collections.Generic;

namespace loan_calculator
{
    public class PaybackPlan
    {
        public List<Double> MonthlyPlan { get; set; }
        public double TotalAmount { get; set; }

        public PaybackPlan(List<Double> monthlyPlan, double totalAmount)
        {
            TotalAmount = totalAmount;
            MonthlyPlan = monthlyPlan;
        }
    }
}
