﻿using System;

namespace loan_calculator
{
    public class LoanFactory
    {
        private double amount;

        private double years;

        private String loanType;

        private String loanPurpose;

        public LoanFactory(double amount, double years, String loanType, String loanPurpose)
        {
            this.amount = amount;
            this.years = years;
            this.loanType = loanType;
            this.loanPurpose = loanPurpose;
        }

        public ILoan CreateLoan()
        {
            double interest = GetLoanInterest(loanPurpose);

            //if (loanType == "SERIES" ){
            ILoan loan = new SeriesLoan(amount, years, interest);
            //}

            return loan;
        }

        private double GetLoanInterest(String loanPurpose) {
            // TODO: get interest from database.

            if (loanPurpose == "HOUSE" ){
                return 3.5;
            } 
            else
            {
                return 10;    
            }
        }
    }
}
