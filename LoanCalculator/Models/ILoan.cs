using System;

namespace loan_calculator
{
    public interface ILoan
    {
        double Interest { get; }

        PaybackPlan GeneratePaybackPlan();
    }
}
