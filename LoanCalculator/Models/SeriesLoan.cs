﻿using System;
using System.Collections.Generic;

namespace loan_calculator
{
    public class SeriesLoan : ILoan
    {
        private double amount;

        private double years;

        public double Interest { get; } 

        public SeriesLoan(double amount, double years, double interest)
        {
            this.amount = amount;
            this.years = years;
            Interest = interest;
        }

        public PaybackPlan GeneratePaybackPlan()
        {

            List<double> plan = new List<double>();
            double monthsLeft = years * 12;
            double totalToPay = 0;
            double amountLeft = amount;
            double monthlyDeduction = amount / monthsLeft;

            while (monthsLeft > 0){
                double thisMonthsInterest = ((amountLeft / 100) * Interest) / 12;
                double thisMonthsPayment = thisMonthsInterest + monthlyDeduction;

                amountLeft -= monthlyDeduction;
                totalToPay += thisMonthsPayment;
                monthsLeft -= 1;
                
                plan.Add(Math.Round(thisMonthsPayment, 2));
            }
            
            PaybackPlan paybackPlan = new PaybackPlan(plan, Math.Round(totalToPay));
            return paybackPlan;
            
        }

    }
}
