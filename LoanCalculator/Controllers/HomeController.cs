﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace loan_calculator.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class PaybackController : ControllerBase
    {
        private readonly ILogger<PaybackController> _logger;

        public PaybackController(ILogger<PaybackController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<PaybackPlan> Get()
        {
            ILoan loan = new LoanFactory(50000, 10, "SERIES", "HOUSE").CreateLoan();
            return loan.GeneratePaybackPlan();
        }
    }
}
