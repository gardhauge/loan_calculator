import React, { Component } from 'react';

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
    this.state = { paybackPlan: [], loading: true };
  }

    componentDidMount() {
    this.populateLoanData();
  }

    static renderPaybackPlan(paybackPlan) {
      console.log(paybackPlan)
;
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Month</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          {paybackPlan.map((planLine, index) => (
            <tr key={`planline${index}`}>
              <td>{index + 1}</td>
              <td>{planLine}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : FetchData.renderPaybackPlan(this.state.paybackPlan.monthlyPlan);

    return (
      <div>
        <h1 id="tabelLabel" >Payback Plan</h1>
        <p>Total amount to pay back: {this.state.paybackPlan.totalAmount}</p>
        {contents}
      </div>
    );
  }

  async populateLoanData() {
    const response = await fetch('payback');
    const data = await response.json();    
    this.setState({ paybackPlan: data, loading: false });
  }
}
