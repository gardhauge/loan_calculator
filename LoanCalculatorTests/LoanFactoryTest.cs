using Microsoft.VisualStudio.TestTools.UnitTesting;
using loan_calculator;
namespace unit_tests
{
    [TestClass]
    public class LoanFactoryTest
    {
        ILoan loan = new LoanFactory(50000, 10, "SERIES", "HOUSE").CreateLoan();       

        [TestMethod]
        public void TestValidateInterestRate()
        {
            Assert.AreEqual(3.5, loan.Interest);
        }

        [TestMethod]
        public void TestValidateCorrectMontlyPayment()
        {
            PaybackPlan plan = loan.GeneratePaybackPlan();

            Assert.AreEqual(561.28, plan.MonthlyPlan[1]);
        }

        [TestMethod]
        public void TestValidateCorrectTotalAmount()
        {
            PaybackPlan plan = loan.GeneratePaybackPlan();

            Assert.AreEqual(58823, plan.TotalAmount);
        }

        [TestMethod]
        public void TestValidateCorrectNumberOfMonts()
        {
            PaybackPlan plan = loan.GeneratePaybackPlan();

            Assert.AreEqual(120, plan.MonthlyPlan.Count);
        }
    }
}
